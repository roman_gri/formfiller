package ru._1243.form_filler.field_definitions;

/**
 * Roman 01.10.2017.
 */
public class FieldDefinitionFactory {

  public static FieldDefinition newFieldDefinition(String placeholder, String value,
      int firstLineLength,
      int lineLength) {
    return new FieldDefinition(placeholder, value, firstLineLength, lineLength);
  }

  private FieldDefinitionFactory() {
  }
}
