package ru._1243.form_filler.field_definitions;

import java.nio.file.Path;

/**
 * Roman 01.10.2017.
 */
public interface FormFiller {

  void fillTemplate(Path template);

  void fillTemplatesInDir(Path templatesDir);
}
