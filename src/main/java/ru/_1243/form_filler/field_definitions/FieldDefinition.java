package ru._1243.form_filler.field_definitions;

/**
 * Roman 01.10.2017.
 */
public class FieldDefinition {

  private String placeholder;
  private String value;
  private int firstLineLength;
  private int lineLength;

  FieldDefinition(String placeholder, String value, int firstLineLength, int lineLength) {
    this.placeholder = placeholder;
    this.value = value;
    this.firstLineLength = firstLineLength;
    this.lineLength = lineLength;
  }

  public String getPlaceholder() {
    return placeholder;
  }

  public String getValue() {
    return value;
  }

  public int getFirstLineLength() {
    return firstLineLength;
  }

  public int getLineLength() {
    return lineLength;
  }
}
