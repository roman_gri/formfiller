package ru._1243.form_filler.field_definitions.reader;

import java.util.List;
import ru._1243.form_filler.field_definitions.FieldDefinition;

/**
 * Roman 01.10.2017.
 */
public interface FieldDefinitionReader {

  List<FieldDefinition> getFieldDefinitions();
}
