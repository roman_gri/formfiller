package ru._1243.form_filler.placeholder_filler;

import java.nio.file.Path;
import java.util.List;
import ru._1243.form_filler.field_definitions.FieldDefinition;

/**
 * Roman 01.10.2017.
 */
public interface PlaceholderFiller {

  void fillTemplate(Path template, List<FieldDefinition> fieldDefinitions);

  void fillTemplatesInDirectory(Path templatesDir, List<FieldDefinition> fieldDefinitions);
}
